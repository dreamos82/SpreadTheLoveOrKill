using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransition : MonoBehaviour
{
    
    private Animator transitionAnimation;
    // Start is called before the first frame update
    void Start()
    {
        transitionAnimation = GetComponent<Animator>();        
    }
    
    public void LoadScene(string sceneName) {
        StartCoroutine(Transition(sceneName));
    }
    
    IEnumerator Transition(string sceneName) {
        transitionAnimation.SetTrigger("end");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
