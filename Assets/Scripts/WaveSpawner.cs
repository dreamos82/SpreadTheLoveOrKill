using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{

    [System.Serializable]
    public class Wave {
        public Enemy[] enemies;
        public int count;
        public float timeBetweenSpawns;
    }

    public Wave[] waves;
    public Transform[] spawnPoints;
    public float timeBetweenWaves;


    private Wave currentWave;
    private int currentWaveIndex;

    private bool finishSpawning;
    
    public GameObject boss;
    public GameObject healthBar;
    
    public Transform bossSpawn;

    private Transform player;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;        
        StartCoroutine(StartNextWave(currentWaveIndex));
    }

    // Update is called once per frame
    void Update()
    {
        if (finishSpawning == true && GameObject.FindGameObjectsWithTag("Enemy").Length == 0 ) {
            finishSpawning = false;            
            if (currentWaveIndex + 1 < waves.Length) {
                currentWaveIndex++;                
                StartCoroutine(StartNextWave(currentWaveIndex));
            } else {
                Debug.Log("Spawning Boss: "  + currentWaveIndex);
                Instantiate(boss, bossSpawn.position, bossSpawn.rotation);
                healthBar.SetActive(true);
            }
        }
    }

    IEnumerator StartNextWave(int index) {
        yield return new WaitForSeconds(timeBetweenWaves);
        StartCoroutine(SpawnWave(index));
    }

    IEnumerator SpawnWave(int index) {
        currentWave = waves[index];

        for(int i=0; i < currentWave.count; i++) {
            if( player == null) {
                yield break;
            }
            
            Enemy randomEnemy = currentWave.enemies[Random.Range(0, currentWave.enemies.Length)];
            Transform randomSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];

            Instantiate(randomEnemy, randomSpawnPoint.position, randomSpawnPoint.rotation);

            if (i == currentWave.count -1) {
                finishSpawning = true;
            } else {
                finishSpawning = false;
            }
            yield return new WaitForSeconds(currentWave.timeBetweenSpawns);
        }
    }
}
