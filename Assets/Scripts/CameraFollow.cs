using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    
    public Transform playerTransform;
    public float speed;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.position = playerTransform.position;
    }

    // Update is called once per frame
    void Update()
    {   
        if(playerTransform != null ) {
            //Clamp function clamps a value between a minimum float and a maximum float value.
            float clampedX = Mathf.Clamp(playerTransform.position.x, minX, maxX);
            float clampedY = Mathf.Clamp(playerTransform.position.y, minY, maxY);
            if(playerTransform != null) {
                //Lerp function parameters are: current position, target position and speed
                //     it allow a smooth movement from a to b
                transform.position = Vector2.Lerp(transform.position, new Vector2(clampedX, clampedY), speed);
            }
        }
        
    }
}
