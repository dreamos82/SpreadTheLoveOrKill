using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Enemy : MonoBehaviour
{

    public int health;

    public float speed;

    public float timeBetweenAttacks;

    public int damage;

    public int pickupChance;

    public bool isBoss;

    public GameObject[] pickups;

    public GameObject deathEffect;
    protected  Slider healthBar;
    public bool canBeCured;


    [HideInInspector]
    public Transform player;

    protected Player playerObject;


    public virtual void Start() {
        GameObject  playerGO = GameObject.FindGameObjectWithTag("Player");
        playerObject = (Player)playerGO.GetComponent(typeof(Player));
        player = playerGO.transform;
        healthBar = FindObjectOfType<Slider>();
        if ( healthBar != null ) {
            healthBar.maxValue = health;
            healthBar.value = health;
        }
    }

    protected virtual void Transform() {
        Debug.Log("Nothing to do here");
    }

    public virtual void TakeDamage(int damageAmount) {
        health -= damageAmount;

        if (healthBar != null) {
            healthBar.value = health;
        }
        if (health <= 0) {
            int pickupValue = Random.Range(0, 101);
            //Debug.Log("pickupValue=" + pickupValue);
            if (pickupValue < pickupChance) {
                GameObject pickupItem = pickups[Random.Range(0, pickups.Length)];
                Instantiate (pickupItem, transform.position, transform.rotation);
            }

            if ( !playerObject.IsBerserk() && canBeCured) {
                Transform();
            } else {
                if (deathEffect != null) {
                    Instantiate(deathEffect, transform.position, Quaternion.identity);
                }
                Destroy(gameObject);
                playerObject.AddKillOrCured();
                playerObject.BerserkChance();
            }

            if( isBoss ) {
                playerObject.Winner();
            }

        }
    }
}
