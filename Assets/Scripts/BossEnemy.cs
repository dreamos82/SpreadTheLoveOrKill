using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : Enemy
{

    public float minX;
    public float maxX;

    public float minY;
    public float maxY;

    public float timeBetweenSummons;
    private float summonTime;

    private Vector2 targetPosition;
    private Animator animator;

    private SceneTransition sceneTransition;

    public Enemy enemyToSummon;
    private int maxMinions = 10;

    public float stopDistance;
    public float attackSpeed;

    private float attackTime;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        float randomX = Random.Range(minX, maxX);
        float randomY = Random.Range(minY, maxY);
        targetPosition = new Vector2(randomX, randomY);
        animator = GetComponent<Animator>();
        sceneTransition = FindObjectOfType<SceneTransition>();
    }


    // Update is called once per frame
    void Update()
    {
        if (player != null) {
            //print("player is not null");
                if (Time.time > summonTime) {
                    // summonTime is when next enemy will be spawned
                    // Every time a new minion is spawned, we update summonTime making sure to wait at least timeBetweenSummons
                    summonTime = Time.time + timeBetweenSummons;
                    //animator.SetTrigger("stomp");
                }
            }

            if (Vector2.Distance(transform.position, player.position) < stopDistance) {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);

                if (Time.time > attackTime) {
                    //Attack
                    // This is a Coroutine, is a function called in an interval of time
                    // We need it since we want an action to be done in few calls of the update function (since is like an animation)
                    StartCoroutine(Attack());
                    attackTime = Time.time + timeBetweenAttacks;
                }

            }
    }

    public void Summon() {
        if( player != null) {
            Instantiate(enemyToSummon, transform.position, transform.rotation);
        }
    }

    public override void TakeDamage(int damageAmount) {

        Debug.Log("Boss hit");

        health -= damageAmount;

        if (healthBar != null) {
            healthBar.value = health;
        }

        if (health <= 0) {

            StartCoroutine(Die());

        }
    }

    IEnumerator Die() {
        //Debug.Log("Coroutine Started");
        animator.SetTrigger("die");
        yield return new WaitForSeconds(4.0f);
        if (deathEffect != null) {
            Debug.Log("Death effect called");
            Instantiate(deathEffect, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);

        if( isBoss ) {
            playerObject.Winner();
        }
    }

    IEnumerator Attack() {
        player.GetComponent<Player>().TakeDamage(damage);
        // We need to animate the action, so we need two positions: startingPosition and targetPosition
        // The target position is the player position.
        Vector2 startingPosition = transform.position;
        Vector2 targetPosition = player.position;
        // We need a percent variable to tell how much of the animation is done so far
        // We start at 0
        float percent = 0;
        while( percent <= 1) {
            percent += Time.deltaTime * attackSpeed;
            // This is the formula for computing the movement
            float formula = (-Mathf.Pow(percent, 2) + percent) * 4;
            transform.position = Vector2.Lerp(startingPosition, targetPosition, formula);
            // It let us run the animation over a period of time.
            yield return null;
        }
    }
}
