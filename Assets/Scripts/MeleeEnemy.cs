using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : Enemy
{
    public float stopDistance;

    private float attackTime;

    public float attackSpeed;
    // Update is called once per frame

    private Animator animator;

    protected AudioSource audioSource;
    protected AudioClip[] audioClips;

    void Update()
    {
        if (player != null) {
            if (Vector2.Distance(transform.position, player.position) > stopDistance) {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            } else {

                if (Time.time > attackTime && health > 0) {
                    //Attack
                    // This is a Coroutine, is a function called in an interval of time
                    // We need it since we want an action to be done in few calls of the update function (since is like an animation)
                    StartCoroutine(Attack());
                    attackTime = Time.time + timeBetweenAttacks;
                }
            }
        }
    }

    public override void Start() {
        base.Start();
        animator = GetComponent<Animator>();
        animator.Rebind();
        audioSource = GetComponent<AudioSource>();
        if( audioSource != null) {
            audioClips = new AudioClip[2];
            audioClips[0] = Resources.Load("Audio/explosion") as AudioClip;
            audioClips[1] = Resources.Load("Audio/cured") as AudioClip;
        }
        //Debug.Log("Name animator: " + animator.runtimeAnimatorController.name);
    }

    protected override void Transform() {
        if(canBeCured) {
            animator.SetTrigger("cured");
        }
        if(audioSource != null) {
            Debug.Log("Playing cured sound");
            audioSource.PlayOneShot(audioClips[1]);
        }
    }

    public void OnDeathAnimationEnd() {
        //Debug.Log("OnDeathAnimationEnd Called");
        if (deathEffect != null) {
                Instantiate(deathEffect, transform.position, Quaternion.identity);
            }

        Destroy(gameObject);
        playerObject.BerserkChance();
    }

    IEnumerator Attack() {
        player.GetComponent<Player>().TakeDamage(damage);
        // We need to animate the action, so we need two positions: startingPosition and targetPosition
        // The target position is the player position.
        Vector2 startingPosition = transform.position;
        Vector2 targetPosition = player.position;
        // We need a percent variable to tell how much of the animation is done so far
        // We start at 0
        float percent = 0;
        while( percent <= 1) {
            percent += Time.deltaTime * attackSpeed;
            // This is the formula for computing the movement
            float formula = (-Mathf.Pow(percent, 2) + percent) * 4;
            transform.position = Vector2.Lerp(startingPosition, targetPosition, formula);
            // It let us run the animation over a period of time.
            yield return null;
        }
    }
}
