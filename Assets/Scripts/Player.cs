using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private enum Parts:int {
        Head,
        Body,
        LeftArm,
        RightArm,
        LeftFoot,
        RightFoot
    };

    private enum State:int {
        Calm,
        Berserk
    };

    public float speed;

    private Rigidbody2D rigidBody;
    private Vector2 moveAmount;
    private Animator animator;

    private bool isBerserk;

    public int health;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public GameObject[] bodyParts;

    public Sprite[] headSprite;
    public Sprite[] bodySprite;
    public Sprite[] leftArmSprite;
    public Sprite[] rightArmSprite;
    public Sprite[] leftFootSprite;
    public Sprite[] rightFootSprite;

    private int[] numberOfKills;

    private SceneTransition sceneTransition;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sceneTransition = FindObjectOfType<SceneTransition>();
        isBerserk = false;
        numberOfKills = new int[2];
        numberOfKills[0] = 0;
        numberOfKills[1] = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        moveAmount = moveInput.normalized * speed;
        if ( moveInput != Vector2.zero ) {
            animator.SetBool("isRunning", true);
        } else {
            animator.SetBool("isRunning", false);
        }
    }

    public void Winner() {
        Debug.Log("[Player] : Berserk Kills: " + numberOfKills[1] + " Normal: " + numberOfKills[0]);
        if ( numberOfKills[0] >= numberOfKills[1] ) {
            sceneTransition.LoadScene("Win");
        } else {
            sceneTransition.LoadScene("WinB");
        }
    }

    public void BerserkChance() {
        float chance = Random.Range(0, 1f);
        if (chance > 0.5) {
            isBerserk = !isBerserk;
            Debug.Log("Will change " + chance);
            Transform();
        } else {
            Debug.Log("Will stay same way " + chance);
        }
    }

    private void Transform() {
        int curState = (isBerserk) ? 1 : 0;
        Debug.Log("[TRANSFORM] curState: " + curState);
        SpriteRenderer partRender = bodyParts[(int)Parts.Head].GetComponent<SpriteRenderer>();
        partRender.sprite = headSprite[curState];
        partRender = bodyParts[(int)Parts.Body].GetComponent<SpriteRenderer>();
        partRender.sprite = bodySprite[curState];
        partRender = bodyParts[(int)Parts.LeftArm].GetComponent<SpriteRenderer>();
        partRender.sprite = leftArmSprite[curState];
        partRender = bodyParts[(int)Parts.RightArm].GetComponent<SpriteRenderer>();
        partRender.sprite = rightArmSprite[curState];
        partRender = bodyParts[(int)Parts.LeftFoot].GetComponent<SpriteRenderer>();
        partRender.sprite = leftFootSprite[curState];
        partRender = bodyParts[(int)Parts.RightFoot].GetComponent<SpriteRenderer>();
        partRender.sprite = rightFootSprite[curState];
    }

    public void ChangeWeapon (Weapon weapon) {
        Weapon curWeapon = GameObject.FindGameObjectWithTag("Weapon").GetComponent<Weapon>();
        Transform curWeaponTrans = curWeapon.transform;
        Destroy(GameObject.FindGameObjectWithTag("Weapon"));
        Instantiate(weapon, curWeaponTrans.position, curWeaponTrans.rotation, transform);
    }

    void FixedUpdate() {
        rigidBody.MovePosition(rigidBody.position + moveAmount * Time.fixedDeltaTime);
    }

    public void TakeDamage(int damageAmount) {
        health -= damageAmount;
        UpdateHealthUi(health);
        if (health <= 0) {
            Destroy(gameObject);
            sceneTransition.LoadScene("Lose");
        }
    }

    public void Heal(int healAmount) {
        //Technically takeDamage with a negative damage should be able to handle that
        // 5 should become a variable maxHealth;
        if (health + healAmount > 5) {
            health = 5;
        } else {
            health += healAmount;
        }
        UpdateHealthUi(health);
    }

    public bool IsBerserk() {
        return isBerserk;
    }

    public void AddKillOrCured() {
        if(isBerserk) {
            numberOfKills[1]++;
        } else {
            numberOfKills[0]++;
        }
    }

    void UpdateHealthUi(int currentHealth) {
        // There is no need  for currenthealth since is a class variable!
        for (int i=0; i < hearts.Length; i++) {
            if ( i < currentHealth){
                hearts[i].sprite = fullHeart;
            } else {
                hearts[i].sprite = emptyHeart;
            }
        }
    }
}
