using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainStory : MonoBehaviour
{

    public string nextSceneName;

    void OnEnable()
    {
        SceneManager.LoadScene(nextSceneName, LoadSceneMode.Single);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
