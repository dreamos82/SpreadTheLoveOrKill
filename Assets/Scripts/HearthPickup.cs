using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HearthPickup : MonoBehaviour
{
    Player player;
    public int healAmount;

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player") {
            player.Heal(healAmount);
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
