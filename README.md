# SpreadTheLoveOrKill

Me experimenting with gamedev following taking a udemy course

Written in Unity.

All sprites are made by me (this is why they look so ugly)

# Copyright Info

Intro songs:

* Intro screens: from zapslat.com
* Sounds effect from freesound.org
    * explosion.wav from user [_inspectorj_](https://freesound.org/people/InspectorJ/sounds/448226/)
    * cured.wav from user [_ryusa_](https://freesound.org/people/ryusa/sounds/531123/)
    * Friendship song: [_huw2k8_](https://freesound.org/people/huw2k8/sounds/646970/)
    * First ending scene: [_chris schum_](https://freesound.org/people/chris_schum/sounds/474145/)
* Alt ending sound from [_Tonisound_](https://pixabay.com/music/metal-shot-155794/)
